"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var dotenv = __importStar(require("dotenv"));
dotenv.config();
var isEnvironment = process.env.NODE_ENV === 'development';
exports.default = {
    host: process.env.HOST || "127.0.0.1",
    environment: process.env.NODE_ENV || "development",
    port: process.env.PORT || 3333,
    db: {
        host: isEnvironment ? process.env.DB_HOST_TEST : process.env.DB_HOST
    },
    auth: {
        secretKey: process.env.SECRET_KEY || "4C31F7EFD6857D91E729165510520424"
    },
    logging: {
        dir: process.env.LOGGING_DIR || "logs",
        level: process.env.LOGGING_LEVEL || "debug"
    }
};
//# sourceMappingURL=index.js.map