"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("../utils/logger");
/**
 * Generic error response middleware for internal server errors.
 *
 * @param  {any} err
 * @param  {Request} req
 * @param  {Response} res
 * @param  {NextFunction} next
 * @returns void
 */
function genericErrorHandler(err, req, res, next) {
    var logger = new logger_1.Logger(__filename);
    logger.error("Error: " + JSON.stringify(err));
    var errCode = err.status || err.code || 500;
    var errorMsg = "";
    if (Array.isArray(err.error)) {
        errorMsg = err.error.map(function (e) { return e.param + ": " + e.msg; }).toString();
    }
    else {
        errorMsg = err.error
            ? err.error.message + " " + (err.error.detail || "")
            : err.message;
    }
    res.status(errCode).json({
        success: false,
        code: errCode,
        message: errorMsg
    });
}
exports.default = genericErrorHandler;
//# sourceMappingURL=genericErrorHandler.middleware.js.map