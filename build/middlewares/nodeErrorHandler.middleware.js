"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function nodeErrorHandler(err) {
    switch (err.code) {
        case "EACCES":
            process.exit(1);
            break;
        case "EADDRINUSE":
            process.exit(1);
            break;
        default:
            throw err;
    }
}
exports.default = nodeErrorHandler;
//# sourceMappingURL=nodeErrorHandler.middleware.js.map