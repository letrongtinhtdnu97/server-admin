"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = __importStar(require("fs"));
var path = __importStar(require("path"));
var winston = __importStar(require("winston"));
var config_1 = __importDefault(require("../../config"));
var _a = config_1.default.logging, level = _a.level, logDir = _a.dir;
var Logger = /** @class */ (function () {
    function Logger(scope) {
        this.scope = Logger.parsePathToScope((scope) ? scope : Logger.DEFAULT_SCOPE);
        if (!fs.existsSync(logDir)) {
            fs.mkdirSync(logDir);
        }
        var currentDate = new Date().toJSON().slice(0, 10).replace(/-/g, '-');
        this.transports = [];
        this.transports.push(new winston.transports.File({ filename: logDir + "/" + level + "-" + currentDate + ".log", level: level }));
        var myFormat = winston.format.printf(function (info) {
            return info.timestamp + " " + info.level + ": " + info.message;
        });
        if (config_1.default.environment !== 'test') {
            this.transports.push(new winston.transports.Console({
                format: winston.format.combine(winston.format.timestamp(), winston.format.colorize({ all: true }), myFormat)
            }));
        }
        var transports = this.transports;
        this.logger = winston.createLogger({
            format: winston.format.combine(winston.format.timestamp({
                format: 'YYYY-MM-DD HH:mm:ss'
            }), myFormat),
            transports: transports
        });
    }
    Logger.parsePathToScope = function (filepath) {
        if (filepath.indexOf(path.sep) >= 0) {
            filepath = filepath.replace(process.cwd(), '');
            filepath = filepath.replace(path.sep + "src" + path.sep, '');
            filepath = filepath.replace(path.sep + "dist" + path.sep, '');
            filepath = filepath.replace('.ts', '');
            filepath = filepath.replace('.js', '');
            filepath = filepath.replace(path.sep, ':');
        }
        return filepath;
    };
    Logger.prototype.debug = function (message) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        this.log('debug', message, args);
    };
    Logger.prototype.info = function (message) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        this.log('info', message, args);
    };
    Logger.prototype.warn = function (message) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        this.log('warn', message, args);
    };
    Logger.prototype.error = function (message) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        this.log('error', message, args);
    };
    Logger.prototype.log = function (level, message, args) {
        var formattedMsg = this.formatScope() + " " + message;
        if (args && args.length > 0) {
            formattedMsg = formattedMsg + JSON.stringify(args);
        }
        this.logger.log(level, formattedMsg);
    };
    Logger.prototype.formatScope = function () {
        return "[" + this.scope + "]";
    };
    Logger.DEFAULT_SCOPE = 'app';
    return Logger;
}());
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map