"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
// controllers
var register_controller_1 = __importDefault(require("./controllers/register.controller"));
var login_controller_1 = __importDefault(require("./controllers/login.controller"));
var logout_controller_1 = __importDefault(require("./controllers/logout.controller"));
//middlewares
var authHandler_middleware_1 = require("./middlewares/authHandler.middleware");
var auth = new authHandler_middleware_1.AuthHandler();
var router = express_1.Router();
router.use('/auth', [register_controller_1.default, login_controller_1.default, logout_controller_1.default]);
exports.default = router;
//# sourceMappingURL=routes.js.map