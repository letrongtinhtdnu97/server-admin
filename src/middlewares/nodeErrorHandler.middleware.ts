export default function nodeErrorHandler(err: NodeJS.ErrnoException): void {
    switch (err.code) {
      case "EACCES":
        process.exit(1);
  
        break;
  
      case "EADDRINUSE":
        process.exit(1);
  
        break;
  
      default:
        throw err;
    }
  }