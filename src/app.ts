import 'reflect-metadata';
import express from 'express'
import morgan from 'morgan';
import bodyParser from "body-parser";


//routes
import routes from './routes'
//database
import { createConnection, getManager } from "typeorm";

//config
import config from './config';
 //logger
import {Logger, ILogger } from './utils/logger'


//middlewares
import nodeErrorHandler from './middlewares/nodeErrorHandler.middleware'
import genericErrorHandler from './middlewares/genericErrorHandler.middleware'
import notFoundHandler from './middlewares/notFoundHandler.middleware'
import {AuthHandler} from './middlewares/authHandler.middleware'
export class Application {
    app: express.Application;
    config = config;
    logger: ILogger; 

    constructor (){
        this.logger = new Logger(__filename)
        this.app = express();
        this.app.use(
            morgan("dev", {
                skip: () => process.env.NODE_ENV === "test"
              })
        )
        this.app.use(bodyParser.json())
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(new AuthHandler().initialize());
        this.app.use('/api',routes)

        //Error 403
        this.app.use(genericErrorHandler);
        this.app.use(notFoundHandler)
    }

    setupDbAndServer = async() => {
        const conn_admin = await createConnection('user');
        this.logger.info(
            `Connected to database. Connection: ${conn_admin.name} / ${
                conn_admin.options.database
            }`
        );
        const conn_user = await createConnection('admin');
        this.logger.info(
            `Connected to database. Connection: ${conn_user.name} / ${
                conn_user.options.database
            }`
        );
        await this.startServer();
    }

    startServer = (): Promise<boolean> => {
        return new Promise((resolve, reject) => {
            this.app.listen(+this.config.port,() => {
                this.logger.info(`Server start at port: ${this.config.port}`);
                resolve(true)
            })
            .on('err',nodeErrorHandler);
        })
    }
}