import {Router} from 'express'

// controllers
import registerRouter from './controllers/register.controller'
import loginRouter from './controllers/login.controller'
import logoutRouter from './controllers/logout.controller'
//middlewares
import { AuthHandler } from './middlewares/authHandler.middleware';

const auth = new AuthHandler();
const router: Router = Router();


router.use('/auth',[registerRouter,loginRouter,logoutRouter]);

export default router;