import {getManager, Repository} from 'typeorm';
import {Logger,ILogger} from '../utils/logger';

import {User} from '../entities_admin/user.entity'
export class UserService {
    userRepository: Repository<User>
    logger: ILogger;

    constructor(){
        this.logger = new Logger(__filename);
        this.userRepository = getManager('admin').getRepository(User);
    }

    instantiate(data: Object): User | undefined {
        return this.userRepository.create(data);
    }

    //insert
    async insert(data: User): Promise<User> {
        this.logger.info("Create a new user", data);
        const newUser = this.userRepository.create(data);
        return await this.userRepository.save(newUser);
    }

    //update
    async update(user: User): Promise<User | undefined> {
        try {
          this.logger.info("Update user", user);
          const updatedUser = await this.userRepository.save(user);
          return updatedUser;
        } catch (error) {
          return Promise.reject(error);
        }
    }

    //getAll
    async getAll(): Promise<User[]> {
        return await this.userRepository.find();
    }


    async getById(id: string | number): Promise<User> {
        if (id) {
          return await this.userRepository.findOne(id);
        }
        return Promise.reject(false);
    }

    async getByLogin(login: string | number): Promise<User | undefined> {
        try {
          const user: User = await this.userRepository.findOne({
            where: {
              login
            }
          });
          if (user) {
            return user;
          } else {
            return undefined;
          }
        } catch (error) {
          return Promise.reject(error);
        }
    }

    async getByEmail(email: string): Promise<User | undefined> {
        try {
          const user: User = await this.userRepository.findOne({
            where: {
              email
            }
          });
          if (user) {
            return user;
          } else {
            return undefined;
          }
        } catch (error) {
          return Promise.reject(error);
        }
    }

    async setLastFailedLoggedDate(user: User): Promise<object> {
        const userId: User = this.userRepository.getId(user);
    
        try {
          return await this.userRepository.update(userId, {
            lastFailedLoggedDate: new Date()
          });
        } catch (error) {
          return Promise.reject(error);
        }
    }

    async setLastPresentLoggedDate(user: User): Promise<object> {
        const userId: User = this.userRepository.getId(user);
    
        try {
          return await this.userRepository.update(userId, {
            lastPresentLoggedDate: new Date()
          });
        } catch (error) {
          return Promise.reject(error);
        }
    }

    async setLastSuccessfulLoggedDate(user: User): Promise<object> {
        const userId: User = this.userRepository.getId(user);
    
        try {
          return await this.userRepository.update(userId, {
            lastSuccessfulLoggedDate: user.lastPresentLoggedDate
          });
        } catch (error) {
          return Promise.reject(error);
        }
    }
}