import bcrypt from "bcryptjs";
import {NextFunction, Response, Request, Router} from 'express';
import { body, param, validationResult } from "express-validator";
import * as HttpStatus from "http-status-codes";
import { getManager } from "typeorm";
import {Logger,ILogger} from '../utils/logger'
//import entities
import {User} from '../entities_admin/user.entity';

//import middlewares
import {AuthHandler} from '../middlewares/authHandler.middleware';

//import services
import {UserService} from '../services/users.service';

//import interface
import {IResponseError} from '../resources/interfaces/IResponseError.interface';

const auth = new AuthHandler();
const logoutRouter: Router = Router();

logoutRouter
    .route('/logout')
    .put(
        auth.authenticate('jwt'),
        async (req: Request, res: Response, next: NextFunction) => {
            const userService = new UserService();
            const logger: ILogger = new Logger(__filename)
            try {
                const user: User = await userService.getById(req.body.id);
                await userService.setLastSuccessfulLoggedDate(user);
                logger.warn(`${user} control logout `, {name: user.name,lastFailedLoggedDate: user.lastFailedLoggedDate })
                return res.status(HttpStatus.OK).json({
                    success: true
                })
            } catch (error) {
                const err: IResponseError = {
                    success: false,
                    code: HttpStatus.BAD_REQUEST,
                    error
                };
                next(err);
            }
        }
    );

export default logoutRouter
