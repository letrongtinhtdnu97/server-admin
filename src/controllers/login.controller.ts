import bcrypt from "bcryptjs";
import {NextFunction, Response, Request, Router} from 'express';
import { body, param, validationResult } from "express-validator";
import * as HttpStatus from "http-status-codes";
import { getManager } from "typeorm";

//import entities
import {User} from '../entities_admin/user.entity';

//import middlewares
import {AuthHandler} from '../middlewares/authHandler.middleware';

//import services
import {UserService} from '../services/users.service'

//import interface
import {IResponseError} from '../resources/interfaces/IResponseError.interface'


const loginRouter: Router = Router()

loginRouter
    .route('/login')
    .post([
        body("login")
            .isNumeric()
            .isLength({min:1, max:20}),
        body("password")
            .isLength({min:6})
    ],
    
    async(req: Request, res: Response, next: NextFunction) => {
        const userService = new UserService();
        const authHandler = new AuthHandler(); 
        const validationErrors = validationResult(req);
        const user: User = await userService.getByLogin(req.body.login);
        
        if(!user){
            const err: IResponseError = {
                success: false,
                code: HttpStatus.UNAUTHORIZED
              };
            return next(err);
        }
        let isPasswordCorrect: boolean =  await bcrypt.compare(
            req.body.password,
            user.password
        );
        
        if (!isPasswordCorrect || !validationErrors.isEmpty()) {
            if (!isPasswordCorrect) await userService.setLastFailedLoggedDate(user);
    
            const err: IResponseError = {
              success: false,
              code: !isPasswordCorrect
                ? HttpStatus.UNAUTHORIZED
                : HttpStatus.BAD_REQUEST
            };
            return next(err);
        }

        try {
            await userService.setLastPresentLoggedDate(user);
            const token: string = authHandler.generateToken(user);
            return res.status(HttpStatus.OK).json({
                success: true,
                token
            });   
          } catch (error) {
              const err: IResponseError = {
                  success: false,
                  code: HttpStatus.BAD_REQUEST,
                  error
              };
              next(err);
        }
    }
)

export default loginRouter;