import {NextFunction, Response, Request, Router} from 'express';
import { body, param, validationResult } from "express-validator";
import * as HttpStatus from "http-status-codes";
import { getManager } from "typeorm";

//import entities
import {User} from '../entities_admin/user.entity';

//import middlewares
import {AuthHandler} from '../middlewares/authHandler.middleware';

//import services
import {UserService} from '../services/users.service'

//import interface
import {IResponseError} from '../resources/interfaces/IResponseError.interface'

const registerRouter: Router = Router();


registerRouter
    .route("/register")
    .post([
        body("name")
            .isLength({min:1,max:255})
            .isAlpha()
            .isString(),
        body("surname")
            .isLength({min:1, max:255})
            .isAlpha()
            .isString(),
        body("email")
            .isEmail(),
        body("login")
            .isNumeric()
            .isLength({min:1, max:20}),
        body("password")
            .isLength({min:1, max: 255})
    ],
    async (req: Request, res: Response, next: NextFunction) => {
        const userService = new UserService();
        
        const validationErrors = validationResult(req);
        
        //check found login and email
        const isLogin: User = await userService.getByLogin(req.body.login);
        const isEmail: User = await userService.getByEmail(req.body.email);

        if (isLogin || isEmail || !validationErrors.isEmpty()) {
            const error: IResponseError = {
              success: false,
              code: HttpStatus.BAD_REQUEST,
              error: validationErrors.array()
            };
            return next(error);
        }
        try {
            let user = new User();
            user.name = req.body.name;
            user.surname = req.body.surname;
            user.email = req.body.email;
            user.login = req.body.login;
            user.password = req.body.password;

            const userRepository = getManager('admin').getRepository(User); 
            user = userRepository.create(user);
            user = await userService.insert(user);

            return res.status(HttpStatus.OK).json({
                success: true
            })
        } catch (error) {
            const err : IResponseError = {
                success: false,
                code: HttpStatus.BAD_REQUEST,
                error
            };
            next(err)
        }
    }
    
);

export default registerRouter;