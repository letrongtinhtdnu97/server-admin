import * as dotenv from 'dotenv'

dotenv.config();

const isEnvironment = process.env.NODE_ENV === 'development' 
export default {
    host: process.env.HOST || "127.0.0.1",
    environment: process.env.NODE_ENV || "development",
    port: process.env.PORT || 3333,
    db: {
        host: isEnvironment ? process.env.DB_HOST_TEST : process.env.DB_HOST
    },
    auth: {
        secretKey: process.env.SECRET_KEY || "4C31F7EFD6857D91E729165510520424"
    },
    logging: {
        dir: process.env.LOGGING_DIR || "logs",
        level: process.env.LOGGING_LEVEL || "debug"
    }
}