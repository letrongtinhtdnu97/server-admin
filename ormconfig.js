module.exports = [{
   type: "postgres",
   host: "localhost",
   name: 'admin',
   port: 5432,
   username: "postgres",
   password: "123123",
   database: "server",
   synchronize: true,
   logging: false,
   entities: [__dirname + "/src/entities_admin/**/*.ts"],
   migrations: [__dirname + "/src/migration_admin/**/*.ts"],
   subscribers: [__dirname + "/src/subscriber_admin/**/*.ts"],
   cli: {
      entitiesDir: __dirname + "/src/entities_admin",
      migrationsDir: __dirname + "/src/migration_admin",
      subscribersDir: __dirname + "/src/subscriber_admin"
    }
},
{
   type: "postgres",
   host: "localhost",
   port: 5432,
   name: 'user',
   username: "postgres",
   password: "123123",
   database: "server_user",
   synchronize: true,
   logging: false,
   entities: [__dirname + "/src/entities_user/**/*.ts"],
   migrations: [__dirname + "/src/migration_user/**/*.ts"],
   subscribers: [__dirname + "/src/subscriber_user/**/*.ts"],
   cli: {
      entitiesDir: __dirname + "/src/entities_user",
      migrationsDir: __dirname + "/src/migration_user",
      subscribersDir: __dirname + "/src/subscriber_user"
    }
},
]